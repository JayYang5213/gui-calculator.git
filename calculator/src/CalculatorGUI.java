
/**
 * @author daniu
 * @program homework
 * @description 计算器
 * @date 2023-07-13 08:49
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorGUI extends JFrame {
    private JTextField inputOutputField; // 输入/输出文本框

    public CalculatorGUI() {
        setTitle("GUI计算器");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // 创建主面板，并设置布局为网格布局
        JPanel mainPanel = new JPanel(new GridLayout(2, 1));

        // 创建输入/输出文本框，并添加到主面板的第一行
        inputOutputField = new JTextField();
        mainPanel.add(inputOutputField);

        // 创建按钮面板，并设置布局为网格布局
        JPanel buttonPanel = new JPanel(new GridLayout(4, 4));

        // 创建按钮，并添加到按钮面板
        addButton(buttonPanel, "9");
        addButton(buttonPanel, "8");
        addButton(buttonPanel, "7");

        addButton(buttonPanel, "/");
        addButton(buttonPanel, "*");

        addButton(buttonPanel, "6");
        addButton(buttonPanel, "5");
        addButton(buttonPanel, "4");

        addButton(buttonPanel, "+");
        addButton(buttonPanel, "-");

        addButton(buttonPanel, "3");
        addButton(buttonPanel, "2");
        addButton(buttonPanel, "1");

        addButton(buttonPanel, "0");
        addButton(buttonPanel, ".");

        addButton(buttonPanel, "AC");
        addButton(buttonPanel, "=");
        addButton(buttonPanel, "C");

        // 将按钮面板添加到主面板的第二行
        mainPanel.add(buttonPanel);

        // 添加主面板到窗口
        add(mainPanel);

        pack(); // 自动调整窗口大小以适应组件
        setSize(400, 600);
        setLocationRelativeTo(null); // 窗口居中显示
        setVisible(true);
    }

    /**
     * 将按钮添加到面板
     *
     * @param panel
     * @param label
     */
    private void addButton(JPanel panel, String label) {
        JButton button = new JButton(label);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 获取按钮上的标签
                String buttonText = button.getText();

                // 根据按钮上的标签执行相应的操作
                switch (buttonText) {
                    case "AC":
                        clearInputOutput();
                        break;
                    case "=":
                        calculateResult();
                        break;
                    case "C":
                        clearOne();
                        break;
                    default:
                        appendToInputOutput(buttonText);
                        break;
                }
            }
        });

        panel.add(button);
    }

    /**
     * 实现C按钮删除末尾字符
     */
    private void clearOne() {
        String str = inputOutputField.getText();
        if (str.length() > 0) {
            str = str.substring(0, str.length() - 1);
            inputOutputField.setText(str);
        }
    }

    /**
     * 实现AC按钮功能
     */
    private void clearInputOutput() {
        inputOutputField.setText(""); // 清空输入/输出文本框
    }

    /**
     * 封装用户输入的内容
     *
     * @param text
     */
    private void appendToInputOutput(String text) {
        String currentText = inputOutputField.getText();
        inputOutputField.setText(currentText + text); // 在输入/输出文本框末尾追加文本
    }

    /**
     * 调用计算的方法（解耦）
     */
    private void calculateResult() {
        String expression = inputOutputField.getText();
        try {
            double result = evaluateExpression(expression);
            inputOutputField.setText(Double.toString(result)); // 显示计算结果
        } catch (Exception e) {
            inputOutputField.setText("计算错误"); // 显示错误消息
        }
    }

    /**
     * 根据用户输入的字符串具体进行计算
     */
    private double evaluateExpression(String expression) {
        // 在这里实现计算表达式的逻辑
        return ExpressionCalculator.calculate(expression);
    }
}
