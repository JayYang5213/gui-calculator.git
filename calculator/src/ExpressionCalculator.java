import java.util.Stack;

/**
 * @author daniu
 * @program homework
 * @description 实现了基本的四则运算逻辑，并通过栈来实现运算符的优先级判断和计算过程
 * @date 2023-07-13 10:44
 */

public class ExpressionCalculator {
    /**
     * 计算器的入口方法，接收一个数学表达式作为输入，返回计算结果
     *
     * @param expression
     * @return
     */
    public static double calculate(String expression) {
        // 移除空格
        expression = expression.replaceAll("\\s+", "");
        // 将表达式拆分成数字和运算符的数组
        String[] tokens = expression.split("(?<=\\d)(?=\\D)|(?<=\\D)(?=\\d)");
        // 存储数字
        Stack<Double> numbers = new Stack<>();
        // 存储运算符
        Stack<Character> operators = new Stack<>();
        /*
         * 遍历拆分后的表达式数组，
         * 如果是数字，则将其转换为 double 类型并入栈，
         * 如果是运算符，则判断其与栈顶运算符的优先级，
         * 如果栈顶运算符的优先级高，则进行计算，否则将当前运算符入栈
         * */
        for (String token : tokens) {
            if (isNumber(token)) {
                double number = Double.parseDouble(token);
                numbers.push(number);
            } else if (isOperator(token)) {
                char operator = token.charAt(0);
                while (!operators.isEmpty() && hasPrecedence(operator, operators.peek())) {
                    evaluateOperation(numbers, operators);
                }
                operators.push(operator);
            }
        }

        // 遍历结束后，如果还有剩余的运算符，则进行计算，直到运算符栈为空
        while (!operators.isEmpty()) {
            evaluateOperation(numbers, operators);
        }

        // 返回栈中最后一个数字，即为计算结果
        return numbers.pop();
    }

    /**
     * 执行具体的运算操作，从数字栈和运算符栈中取出数字和运算符进行计算，并将结果压入数字栈
     *
     * @param numbers
     * @param operators
     */
    private static void evaluateOperation(Stack<Double> numbers, Stack<Character> operators) {
        double num2 = numbers.pop();
        double num1 = numbers.pop();
        char operator = operators.pop();
        double result;

        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }

        numbers.push(result);
    }

    /**
     * 判断给定的字符串是否是数字
     *
     * @param token
     * @return
     */
    private static boolean isNumber(String token) {
        return token.matches("\\d+(\\.\\d+)?");
    }

    /**
     * 判断给定的字符串是否是运算符
     *
     * @param token
     * @return
     */
    private static boolean isOperator(String token) {
        return token.matches("[+\\-*/]");
    }

    /**
     * 判断两个运算符的优先级，返回 true 表示 operator1 的优先级高于 operator2
     *
     * @param operator1
     * @param operator2
     * @return
     */
    private static boolean hasPrecedence(char operator1, char operator2) {
        return (operator1 == '*' || operator1 == '/') && (operator2 == '+' || operator2 == '-');
    }
}
