import javax.swing.*;

/**
 * @author daniu
 * @program homework
 * @description 测试类
 * @date 2023-07-13 11:22
 */

public class test {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CalculatorGUI();
            }
        });
    }
}
